#include <stdlib.h>
#include <stdio.h>

typedef struct
{
    int estado;
    char escreve;
    char comando;
} tupla;

void main()
{
    int D = 0, E = 1, R = 2;
    // 0 1 X Y > ~
    int alfabeto[127];
    alfabeto['0'] = 0;
    alfabeto['1'] = 1;
    alfabeto['X'] = 2;
    alfabeto['Y'] = 3;
    alfabeto['>'] = 4;
    alfabeto['~'] = 5;

    tupla tabela[10][10];

    ///// 0
    tabela[0][0].estado = 1;
    tabela[0][0].escreve = 'X';
    tabela[0][0].comando = D;

    tabela[1][0].estado = 1;
    tabela[1][0].escreve = '0';
    tabela[1][0].comando = D;

    tabela[2][0].estado = 2;
    tabela[2][0].escreve = '0';
    tabela[2][0].comando = E;

    tabela[3][0].estado = -1;
    tabela[3][0].escreve = '0';
    tabela[3][0].comando = 0;

    ///// 1

    tabela[0][1].estado = -1;
    tabela[0][1].escreve = '0';
    tabela[0][1].comando = 0;

    tabela[1][1].estado = 2;
    tabela[1][1].escreve = 'Y';
    tabela[1][1].comando = E;

    tabela[2][1].estado = -1;
    tabela[2][1].escreve = '0';
    tabela[2][1].comando = 0;

    tabela[3][1].estado = -1;
    tabela[3][1].escreve = '0';
    tabela[3][1].comando = 0;

    ///// X

    tabela[0][2].estado = -1;
    tabela[0][2].escreve = '0';
    tabela[0][2].comando = 0;

    tabela[1][2].estado = -1;
    tabela[1][2].escreve = '0';
    tabela[1][2].comando = 0;

    tabela[2][2].estado = 0;
    tabela[2][2].escreve = 'X';
    tabela[2][2].comando = D;

    tabela[3][2].estado = -1;
    tabela[3][2].escreve = '0';
    tabela[3][2].comando = 0;

    ///// Y

    tabela[0][3].estado = 3;
    tabela[0][3].escreve = 'Y';
    tabela[0][3].comando = D;

    tabela[1][3].estado = 1;
    tabela[1][3].escreve = 'Y';
    tabela[1][3].comando = D;

    tabela[2][3].estado = 2;
    tabela[2][3].escreve = 'Y';
    tabela[2][3].comando = E;

    tabela[3][3].estado = 3;
    tabela[3][3].escreve = 'Y';
    tabela[3][3].comando = D;

    ///// >

    tabela[0][4].estado = 0;
    tabela[0][4].escreve = '>';
    tabela[0][4].comando = D;

    tabela[1][4].estado = -1;
    tabela[1][4].escreve = '0';
    tabela[1][4].comando = 0;

    tabela[2][4].estado = -1;
    tabela[2][4].escreve = '0';
    tabela[2][4].comando = 0;

    tabela[3][4].estado = -1;
    tabela[3][4].escreve = '0';
    tabela[3][4].comando = 0;

    ///// ~

    tabela[0][5].estado = -1;
    tabela[0][5].escreve = '0';
    tabela[0][5].comando = 0;

    tabela[1][5].estado = -1;
    tabela[1][5].escreve = '0';
    tabela[1][5].comando = 0;

    tabela[2][5].estado = -1;
    tabela[2][5].escreve = '0';
    tabela[2][5].comando = 0;

    tabela[3][5].estado = 4;
    tabela[3][5].escreve = '~';
    tabela[3][5].comando = R;

    char entrada[255];
    int posicao_cabecote;
    int qX;
    int caractereAtual;
    int comandoAtual;

    while (1 != 0)
    {
        gets(entrada);
        posicao_cabecote = 0;
        qX = 0;
        caractereAtual = 0;
        comandoAtual = 0;
        while (comandoAtual != R)
        {

            caractereAtual = entrada[posicao_cabecote];
            comandoAtual = tabela[qX][alfabeto[caractereAtual]].comando;
            entrada[posicao_cabecote] = tabela[qX][alfabeto[caractereAtual]].escreve;

            if (comandoAtual == D)
            {
                posicao_cabecote++;
            }
            else if (comandoAtual == E)
            {
                posicao_cabecote--;
            }
            else if (comandoAtual == R)
            {
                comandoAtual = R;
                printf("RECONHECIDO");
            }

            qX = tabela[qX][alfabeto[caractereAtual]].estado;
            printf("%s\n", entrada);
        };
    }

}
